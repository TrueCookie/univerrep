#include <iostream>
#include "plant/Plant.h"

int main() {
    double height = 28.5;
    auto *percent = new Percent(40);

    auto *plant = new Plant("Sukkulent", height, 4, percent);

    printf("Hydration of %s is %d%%\n", plant->getName().c_str(), plant->getHydration().getValue());

    auto *newPlant = new Plant(*plant);
    newPlant->setName("Monstera");

    printf("Height of %s:", newPlant->getName().c_str());
    newPlant->show_height();
    printf("\n");

    auto brandNewPlant = new Plant();
    printf("Height of %s:", brandNewPlant->getName().c_str());
    brandNewPlant->show_height();
    printf("\n");

    return 0;
}
