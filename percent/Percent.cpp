//
// Created by max on 2/1/2021.
//

#include "Percent.h"

Percent::Percent() {
    value = 0;
}

Percent::Percent(int value) {
    this->value = value;
}

int Percent::getValue() const {
    return value;
}

void Percent::setValue(int value) {
    if(value >= 0 && value <= 100){
        Percent::value = value;
    }
    else{
        throw std::range_error("wrong value");
    }
}

bool Percent::operator+(const Percent &rhs) const {
    return value + rhs.value;
}

