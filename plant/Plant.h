//
// Created by max on 2/1/2021.
//

#ifndef UNIVERREP_PLANT_H
#define UNIVERREP_PLANT_H
#include <iostream>
#include "../percent/Percent.h"

class Plant {

private:
    const std::string DEFAULT_NAME = "<plant_name>";

    std::string name;
    double height;
    int leafNum;
    Percent hydration;

public:
    Plant();
    Plant(const Plant& plant);
    Plant(std::string name, double height, int leafNum, const Percent* hydration);
    virtual ~Plant();

    void show_height() const;

    const Percent &getHydration() const;

    const std::string & getName() const;

    void setName(const std::string &name);
};


#endif //UNIVERREP_PLANT_H
