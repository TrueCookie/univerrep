//
// Created by max on 2/1/2021.
//
#include "Plant.h"

Plant::Plant() {
    this->name = this->DEFAULT_NAME;
    this->height = 0;
    this->leafNum = 0;
    this->hydration = 30;
}

Plant::Plant(const Plant& plant) {
    this->name = plant.name;
    this->height = plant.height;
    this->leafNum = plant.leafNum;
    this->hydration = plant.hydration;
}

Plant::Plant(std::string name, double height, int leafNum, const Percent* hydration) {
    this->name = std::move(name);
    this->height = height;
    this->leafNum = leafNum;
    this->hydration = *hydration;
}

void Plant::show_height() const {
    std::cout << "[INFO: showing height]" << std::endl;
    std::cout << this->height << std::endl;
}

const Percent &Plant::getHydration() const {
    std::cout << "[INFO: getting hydration]" << std::endl;
    return hydration;
}

const std::string &Plant::getName() const {
    std::cout << "[INFO: getting name]" << std::endl;
    return name;
}

void Plant::setName(const std::string &name) {
    std::cout << "[INFO: setting name]" << std::endl;
    this->name = name;
}

Plant::~Plant() = default;
