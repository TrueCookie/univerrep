//
// Created by max on 2/1/2021.
//

#ifndef UNIVERREP_PERCENT_H
#define UNIVERREP_PERCENT_H
#include <iostream>
#include <stdexcept>

class Percent {
private:
    int value;

public:
    Percent();
    Percent(int value);

    bool operator+(const Percent &rhs) const;

    int getValue() const;

    void setValue(int value);

};


#endif //UNIVERREP_PERCENT_H
